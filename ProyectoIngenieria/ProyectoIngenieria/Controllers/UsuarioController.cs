﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using ProyectoIngenieria.Models;
using ProyectoIngenieria.Models.EntityFramework;
using ProyectoIngenieria.Models.ViewModels;
using ProyectoIngenieria.Models.ViewModels.Ahorros;
using ProyectoIngenieria.Models.ViewModels.Creditos;

namespace ProyectoIngenieria.Controllers {
    /// <summary>
    /// Controlador para gestionar el inicio de sesiñon d eun usuario y sus opciones, ademas de la recuperación de contraseña
    /// </summary>
    public class UsuarioController : Controller {
        private ingenieriaso2020Entities ModeloBD;

        public UsuarioController() {
            // Instanciar modelo de la DB EF
            ModeloBD = new ingenieriaso2020Entities();
        }

        [HttpGet]
        public ActionResult Index() {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion"); }

            string TipoUsuario = Convert.ToString(Session["TipoUsuario"]);
            var Persona = (TBL_PERSONAS)Session["DatosPersona"];

            if (TipoUsuario.Equals("Cliente")) {
                var AhorrosClienteBD = ModeloBD.TBL_AHORROS_CLIENTE.Where(x => x.IdCliente == Persona.Id).ToList();
                IEnumerable<AhorrosClienteViewModel> AhorrosCliente = AhorrosClienteBD.Select(x => new AhorrosClienteViewModel(x)).ToList();
                ViewBag.Ahorros = AhorrosCliente;

                var CreditosClienteBD = ModeloBD.TBL_CREDITOS_CLIENTE.Where(x => x.IdCliente == Persona.Id).ToList();
                IEnumerable<CreditosClienteViewModel> CreditosCliente = CreditosClienteBD.Select(x => new CreditosClienteViewModel(x)).ToList();
                ViewBag.Creditos = CreditosCliente;
            }

            return View();
        }

        [HttpGet]
        public ActionResult InicioSesion() {
            if (ValidarSiEstaLogueado()) { return RedirectToAction("Index"); } else { return View(); }
        }

        [HttpPost]
        public ActionResult IniciarSesion(IniciarSesionViewModel Modelo) {
            var Usuario = ModeloBD.TBL_USUARIOS.Where(x => x.Nombre.Equals(Modelo.NombreUsuario) && x.Pass.Equals(Modelo.Contrasena)).FirstOrDefault();

            if (Usuario != null) {
                var TipoUsuario = ModeloBD.TBL_TIPOS_USUARIO.Where(x => x.Id == Usuario.IdTipoUsuario).FirstOrDefault();

                this.Session.Add("Logueado", true);
                this.Session.Add("TipoUsuario", TipoUsuario.Descripcion);

                var Persona = ModeloBD.TBL_PERSONAS.Where(x => x.Id == Usuario.IdPersona).FirstOrDefault();
                this.Session.Add("DatosUsuario", Usuario);
                this.Session.Add("DatosPersona", Persona);

                // Redireccionar a la pagina de inicio de usuario
                return RedirectToAction("Index");
            }
            else {
                ModelState.AddModelError("", "El usuario o contraseña son incorrectos.");
                return View("InicioSesion", Modelo);
            }
        }

        [HttpPost]
        public ActionResult CerrarSesion() {
            this.Session.Add("Logueado", false);
            this.Session.Add("TipoUsuario", null);

            this.Session.Add("DatosUsuario", null);
            this.Session.Add("DatosPersona", null);

            return View("InicioSesion");
        }

        public bool ValidarSiEstaLogueado() {
            bool UsuarioLogueado = Convert.ToBoolean(this.Session["Logueado"]);
            return UsuarioLogueado;
        }

        public ActionResult CambioPass() {
            return View();
        }

        //Consultamos el correo por medio del nombre de usuario
        [HttpPost]
        public ActionResult ConsultarCorreoPorUsuario(IniciarSesionViewModel ModeloVista) {
            #region VARIABLES
            string Mensaje = string.Empty;
            int filas = 0;
            string NuevoPass = string.Empty;
            string Receptor, Asunto, MensajeCorreo;
            #endregion
            try {
                //Consultmaos en la db los datos del usuario para enviar el SMS
                List<SP_RetornarCorreoPorUsuario_Result> DatosUsuario =
                    ModeloBD.SP_RetornarCorreoPorUsuario(ModeloVista.NombreUsuario).ToList();
                //Validamos que tengan datos de lo contrario es un usuario no registrado
                if (DatosUsuario.Count != 0) {
                    //Generamos la nueva contraseña
                    NuevoPass = GenerarPassword(10);
                    //Actualizamos la contraseña del usario
                    filas = ModeloBD.SP_ActualizarContraseña(DatosUsuario[0].Id, NuevoPass);

                    if (filas > 0) {
                        //Enviamos el correo
                        Receptor = DatosUsuario[0].Email;
                        Asunto = "Cambio de contraseña";
                        MensajeCorreo = "Se actualizó su contraseña, por motivos de seguridad no comparta con terceras personas su nueca contraseña, ahora dirijase a inicio de sesión para hacer uso de la plataforma,\n" +
                            "su nueva contraseña es: " + NuevoPass;
                        if (EnviarCorreoElectronico(Receptor, Asunto, MensajeCorreo).Equals("Exito")) {
                            Mensaje = "Exito";
                        }

                    }
                }
            }
            catch (Exception) {
                Mensaje = "Error";
            }

            return Json(new {
                resultado = Mensaje
            });
        }

        public string EnviarCorreoElectronico(string Receptor, string Asunto, string Mensaje) {
            string Estado = "Exito";
            try {
                /* VARIABLES */
                string Emisor = "pruebasuniversidad2020@gmail.com";
                string Contraseña = "1234yalpstHASFGbsdh";
                string Host = "smtp.gmail.com";
                int Puerto = 25;

                /* DATOS DEL CORREO */
                MailMessage Correo = new MailMessage();
                Correo.From = new MailAddress(Emisor);
                Correo.To.Add(Receptor);
                Correo.Subject = Asunto;
                Correo.Body = Mensaje;
                Correo.IsBodyHtml = true;
                Correo.Priority = MailPriority.Normal;
                /* DATOS DEL SMTP */
                SmtpClient smtp = new SmtpClient();
                smtp.Host = Host;
                smtp.Port = Puerto;
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = new System.Net.NetworkCredential(Emisor, Contraseña);
                smtp.Send(Correo);
            }
            catch (Exception error) {
                string m = error.Message;
            }
            return Estado;
        }

        public string GenerarPassword(int longitud) {
            string contraseña = string.Empty;
            string[] letras = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
                                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
            Random EleccionAleatoria = new Random();

            for (int i = 0; i < longitud; i++) {
                int LetraAleatoria = EleccionAleatoria.Next(0, 100);
                int NumeroAleatorio = EleccionAleatoria.Next(0, 9);

                if (LetraAleatoria < letras.Length) {
                    contraseña += letras[LetraAleatoria];
                }
                else {
                    contraseña += NumeroAleatorio.ToString();
                }
            }
            return contraseña;
        }
    }
}