﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProyectoIngenieria.Models.EntityFramework;
using ProyectoIngenieria.Models.ViewModels.Creditos;

namespace ProyectoIngenieria.Controllers {
    public class CreditosController : Controller {
        private ingenieriaso2020Entities ModeloBD;

        public CreditosController() {
            // Instanciar modelo de la DB EF
            ModeloBD = new ingenieriaso2020Entities();
        }

        [HttpGet]
        public async Task<ActionResult> Index() {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            var ListaCreditosBD = await ModeloBD.TBL_CATALOGO_CREDITOS.ToListAsync();
            IEnumerable<IndexCreditosViewModel> Modelo = ListaCreditosBD.Select(x => new IndexCreditosViewModel(x)).ToList();
            return View(Modelo);
        }

        [HttpGet]
        public ActionResult Create() {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            return View();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Nombre,Descripcion,Tasa,Plazo,Estado")] AgregarCreditosViewModel modelo) {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            if (!ModelState.IsValid) {
                ModelState.AddModelError("", "Error, el modelo no es válido");

                return View(modelo);
            }

            var Credito = new TBL_CATALOGO_CREDITOS();
            Credito.Nombre = modelo.Nombre;
            Credito.Descripcion = modelo.Descripcion;
            Credito.Tasa = modelo.Tasa;
            Credito.PlazoMaximoEnMeses = modelo.Plazo;
            Credito.Activo = modelo.Estado;

            Credito = ModeloBD.TBL_CATALOGO_CREDITOS.Add(Credito);
            int RegistrosAfectados = await ModeloBD.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Edit(string id) {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            if (string.IsNullOrEmpty(id)) {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                RedirectToAction("Index");
            }

            // TODO: Validar si el id es un numero mayor a 0

            int IdCredito = Convert.ToInt16(id);

            TBL_CATALOGO_CREDITOS creditoBD = await ModeloBD.TBL_CATALOGO_CREDITOS.FindAsync(IdCredito);

            if (creditoBD == null) {
                return HttpNotFound();
            }

            EditarCreditosViewModel Modelo = new EditarCreditosViewModel(creditoBD);

            return View(Modelo);
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Nombre,Descripcion,Tasa,Plazo,Estado")] EditarCreditosViewModel modelo) {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            if (!ModelState.IsValid) {
                ModelState.AddModelError("", "Error, el modelo no es válido");

                return View(modelo);
            }

            var creditoBD = ModeloBD.TBL_CATALOGO_CREDITOS.Where(x => x.Id == modelo.Id).FirstOrDefault();

            creditoBD.Nombre = modelo.Nombre;
            creditoBD.Descripcion = modelo.Descripcion;
            creditoBD.Tasa = modelo.Tasa;
            creditoBD.PlazoMaximoEnMeses = modelo.Plazo;
            creditoBD.Activo = modelo.Estado;

            ModeloBD.Entry(creditoBD).State = EntityState.Modified;

            int registrosAfectados = await ModeloBD.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Delete(string id) {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            if (string.IsNullOrEmpty(id)) {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                RedirectToAction("Index");
            }

            // TODO: Validar si el id es un numero mayor a 0

            int IdCredito = Convert.ToInt16(id);

            TBL_CATALOGO_CREDITOS creditoBD = await ModeloBD.TBL_CATALOGO_CREDITOS.FindAsync(IdCredito);

            if (creditoBD == null) {
                return HttpNotFound();
            }

            EditarCreditosViewModel Modelo = new EditarCreditosViewModel(creditoBD);

            return View(Modelo);

        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id) {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            TBL_CATALOGO_CREDITOS creditoBD = await ModeloBD.TBL_CATALOGO_CREDITOS.FindAsync(id);
            ModeloBD.TBL_CATALOGO_CREDITOS.Remove(creditoBD);
            await ModeloBD.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                ModeloBD.Dispose();
            }
            base.Dispose(disposing);
        }

        public bool ValidarSiEstaLogueado() {
            bool UsuarioLogueado = Convert.ToBoolean(this.Session["Logueado"]);
            return UsuarioLogueado;
        }
    }
}
