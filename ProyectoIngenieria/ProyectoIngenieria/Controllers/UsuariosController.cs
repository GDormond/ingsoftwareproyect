﻿using ProyectoIngenieria.Models.EntityFramework;
using ProyectoIngenieria.Models.ViewModels.Usuarios;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoIngenieria.Controllers {
    /// <summary>
    /// Controlador para gestionar los usuarios del sistema
    /// </summary>
    public class UsuariosController : Controller {
        private ingenieriaso2020Entities ModeloBD;

        public UsuariosController() {
            // Instanciar modelo de la DB EF
            ModeloBD = new ingenieriaso2020Entities();
        }

        [HttpGet]
        public ActionResult Index() {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            var ListaUsuarios = ModeloBD.TBL_PERSONAS.ToList();
            var Modelo = ListaUsuarios.Select(x => new IndexUsuariosViewModel(x)).ToList();
            return View(Modelo);
        }

        [HttpGet]
        public ActionResult ConfirmacionDatosGuardados() {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            return View();
        }

        [HttpGet]
        public ActionResult Agregar() {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            CargarListaTiposUsuario();

            return View();
        }

        [HttpPost]
        public ActionResult Agregar(AgregarUsuariosViewModel modelo) {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            if (!ModelState.IsValid) {
                ModelState.AddModelError("", "Error, el modelo no es válido");

                CargarListaTiposUsuario();

                return View(modelo);
            }

            var Persona = new TBL_PERSONAS();
            Persona.Cedula = modelo.Cedula;
            Persona.Nombre = modelo.Nombre;
            Persona.Apellido1 = modelo.Apellido1;
            Persona.Apellido2 = modelo.Apellido2;
            Persona.Email = modelo.Correo;
            Persona.Telefono1 = modelo.Telefono;

            var Usuario = new TBL_USUARIOS();
            Usuario.Nombre = modelo.NombreUsuario;
            Usuario.Pass = modelo.Contrasena;
            Usuario.Activo = true;
            Usuario.IdTipoUsuario = modelo.TipoUsuario;

            // Guardar en la base de datos la información de persona
            Persona = ModeloBD.TBL_PERSONAS.Add(Persona);
            var RegistrosAfectados = ModeloBD.SaveChanges();

            // Una vez que está guardado en la bd, se le asignó un Id a persona mediante Identity, tomar ese Id y 
            // colocarlo en la propiedad IdPersona del objeto usuario
            Usuario.IdPersona = Persona.Id;

            // Guardar en la base de datos la información de usuario
            Usuario = ModeloBD.TBL_USUARIOS.Add(Usuario);
            RegistrosAfectados = ModeloBD.SaveChanges();

            return RedirectToAction("ConfirmacionDatosGuardados");
        }

        [HttpGet]
        public ActionResult Editar(string id) {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            if (string.IsNullOrEmpty(id)) {
                RedirectToAction("Index");
            }

            // TODO: Validar si el id es un numero mayor a 0

            int idUsuario = Convert.ToInt16(id);

            var usuarioBD = ModeloBD.TBL_USUARIOS.Where(x => x.Id == idUsuario).FirstOrDefault();

            EditarUsuariosViewModel modelo = new EditarUsuariosViewModel(usuarioBD);

            CargarListaTiposUsuario();

            return View(modelo);
        }

        [HttpPost]
        public ActionResult Editar(EditarUsuariosViewModel modelo) {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            if (!ModelState.IsValid) {
                ModelState.AddModelError("", "Error, el modelo no es válido");

                CargarListaTiposUsuario();

                return View(modelo);
            }

            var usuarioBD = ModeloBD.TBL_USUARIOS.Where(x => x.Id == modelo.IdUsuario).FirstOrDefault();
            var personaBD = ModeloBD.TBL_PERSONAS.Where(x => x.Id == modelo.IdPersona).FirstOrDefault();

            // Datos de usuario
            usuarioBD.Nombre = modelo.NombreUsuario;
            usuarioBD.IdTipoUsuario = modelo.TipoUsuario;

            if (!string.IsNullOrEmpty(modelo.Contrasena)) {
                usuarioBD.Pass = modelo.Contrasena;
            }

            // Datos de persona
            personaBD.Nombre = modelo.Nombre;
            personaBD.Apellido1 = modelo.Apellido1;
            personaBD.Apellido2 = modelo.Apellido2;
            personaBD.Cedula = modelo.Cedula;
            personaBD.Email = modelo.Correo;
            personaBD.Telefono1 = modelo.Telefono;

            ModeloBD.Entry(usuarioBD).State = EntityState.Modified;
            ModeloBD.Entry(personaBD).State = EntityState.Modified;

            int RegistrosAfectados = ModeloBD.SaveChanges();

            return RedirectToAction("ConfirmacionDatosGuardados");
        }

        [HttpGet]
        public ActionResult Eliminar(string id) {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            if (string.IsNullOrEmpty(id)) {
                RedirectToAction("Index");
            }

            // TODO: Validar si el id es un numero mayor a 0

            int idUsuario = Convert.ToInt16(id);

            var usuarioBD = ModeloBD.TBL_USUARIOS.Where(x => x.Id == idUsuario).FirstOrDefault();

            EditarUsuariosViewModel modelo = new EditarUsuariosViewModel(usuarioBD);

            return View(modelo);
        }

        [HttpPost, ActionName("Eliminar")]
        [ValidateAntiForgeryToken]
        public ActionResult EliminarConfirmado(int IdUsuario) {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            var usuarioBD = ModeloBD.TBL_USUARIOS.Where(x => x.Id == IdUsuario).FirstOrDefault();
            var personaBD = ModeloBD.TBL_PERSONAS.Where(x => x.Id == usuarioBD.IdPersona).FirstOrDefault();

            ModeloBD.TBL_USUARIOS.Remove(usuarioBD);
            ModeloBD.TBL_PERSONAS.Remove(personaBD);

            int RegistrosAfectados = ModeloBD.SaveChanges();

            return RedirectToAction("Index");
        }

        private void CargarListaTiposUsuario() {
            // Obtener los tipos de usuario para enviarlo a la vista y llenar el select de tipos de usuario
            var TiposUsuario = ModeloBD.TBL_TIPOS_USUARIO.ToList();
            ViewBag.ListaTiposUsuario = TiposUsuario;
        }

        public bool ValidarSiEstaLogueado() {
            bool UsuarioLogueado = Convert.ToBoolean(this.Session["Logueado"]);
            return UsuarioLogueado;
        }
    }
}