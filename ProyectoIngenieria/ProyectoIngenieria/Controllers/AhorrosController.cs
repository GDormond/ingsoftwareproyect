﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using ProyectoIngenieria.Models.EntityFramework;
using ProyectoIngenieria.Models.ViewModels.Ahorros;
namespace ProyectoIngenieria.Controllers
{
    public class AhorrosController : Controller
    {
        //Instancia
        #region Instancia
        ingenieriaso2020Entities ModeloDB = new ingenieriaso2020Entities();
        #endregion
        // GET: Ahorros
        public ActionResult Index()
        {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }
            return View();
        }

        public ActionResult ModificarAhorro(int Id)
        {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            AhorroCatalogo ahorro = new AhorroCatalogo();
            ahorro = ConsultaCatalogoAhorrosID(Id).FirstOrDefault();
           
            return View(ahorro);
        }

        public ActionResult EliminarAhorro(int Id)
        {
            if (!ValidarSiEstaLogueado()) { return RedirectToAction("InicioSesion", "Usuario"); }

            int Filas = EliminarCatalogoAhorroId(Id);
            if (Filas > 0)
            {
                this.ViewBag.Mensaje = "Catalogo de ahorro se a eliminado";
            }
            else
            {
                this.ViewBag.Mensaje = "No se pudo eliminar el catalogo";
            }
            return View();
        }

        public bool ValidarSiEstaLogueado()
        {
            bool UsuarioLogueado = Convert.ToBoolean(this.Session["Logueado"]);
            return UsuarioLogueado;
        }

        //Metodo consulta la lista de los catalogos
        public ActionResult ConsultaCatalogoAhorros()
        {
            var Estado = "";

            var Lista = (from x in ModeloDB.TBL_CATALOGO_AHORROS
                         select new
                         {
                             x.Id,
                             x.Nombre,
                             x.Descripcion,
                             x.TasaInteres,
                             Estado = x.Activo == true ? "Activo" : "Desactivo",
                             x.ComisionRetiroAnticipado
                         });
            return Json(new
            {
                resultado = Lista
            });
        }

        public List<AhorroCatalogo> ConsultaCatalogoAhorrosID(int Id)
        {
            AhorroCatalogo Datos = new AhorroCatalogo();
            List<AhorroCatalogo> Lista = new List<AhorroCatalogo>();
            var DatosConsulta = (from x in ModeloDB.TBL_CATALOGO_AHORROS
                                 where x.Id == Id
                                 select new
                                 {
                                     x.Id,
                                     x.Nombre,
                                     x.Descripcion,
                                     x.TasaInteres,
                                     x.Activo,
                                     x.PermiteRetiroAnticipado,
                                     x.ComisionRetiroAnticipado
                                 }).FirstOrDefault();

            Datos.Id = DatosConsulta.Id;
            Datos.Nombre = DatosConsulta.Nombre;
            Datos.Descripcion = DatosConsulta.Descripcion;
            Datos.TasaInteres = DatosConsulta.TasaInteres;
            Datos.Activo = DatosConsulta.Activo;
            Datos.Anticipado = DatosConsulta.PermiteRetiroAnticipado;
            Datos.Comision = DatosConsulta.ComisionRetiroAnticipado;

            Lista.Add(Datos);

            return Lista;
        }

        //Metodo actualiza el catalogo de ahorro

        public ActionResult ActualizarCatalogoAhorroID(AhorroCatalogo ModeloVista)
        {
            int FilasAfectadas = 0;
            string error = "";
            try
            {
                FilasAfectadas = this.ModeloDB.SP_ACTUALZIAR_CATALOGO_AHORRO_ID(ModeloVista.Id,
                                                                                ModeloVista.Nombre,
                                                                                ModeloVista.Descripcion,
                                                                                Convert.ToInt32(ModeloVista.TasaInteres),
                                                                                ModeloVista.Activo,
                                                                                ModeloVista.Anticipado,
                                                                                ModeloVista.Comision);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                if (FilasAfectadas == 0)
                {
                    error = "No se pudo actualizar los datos";
                }
            }

            return Json(new { 
                resultado = FilasAfectadas,
                mensaje = error
            });
        }

        public int EliminarCatalogoAhorroId(int id)
        {
            int filasAfectadas = 0;
            try
            {
                filasAfectadas = this.ModeloDB.SP_ELIMINAR_CATALOGO_AHORRO_ID(id);
            }
            catch (Exception ex)
            {

            }

            return filasAfectadas;
        }

        public ActionResult RegistrarCatalogoAhorro(AhorroCatalogo ModeloVista)
        {
            string mensaje = "";
            int FilasAfectadas = 0;

            try
            {
                FilasAfectadas = this.ModeloDB.SP_REGISTRAR_CATALOGO_AHORRO_(ModeloVista.Nombre,
                                                                             ModeloVista.Descripcion,
                                                                             Convert.ToInt32(ModeloVista.TasaInteres),
                                                                             ModeloVista.Activo,
                                                                             ModeloVista.Anticipado,
                                                                             ModeloVista.Comision);
            }
            catch (Exception err)
            {
                FilasAfectadas = -1;
                mensaje = err.Message;
            }
            finally 
            {
                if (FilasAfectadas > 0)
                {
                    mensaje = "Registro de ahorro con exito";
                }           
            
            }

            return Json(new {
                filas =FilasAfectadas,
                resultado = mensaje
            });
        }
    }
}