﻿$(function () {
    creaValidaciones();
    EventoClick();
});

function MensajeError(Mensaje) {
    Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: Mensaje
    })
}



///crea las validaciones para el formulario
function creaValidaciones() {
    $("#frmModifica").validate(
        ///objeto que contiene "las condiciones" que el formulario
        ///debe cumplir para ser considerado válido
        {
            rules: {
                Nombre: {
                    required: true,
                    maxlength: 1000,
                    minlength: 8
                },
                Descripcion: {
                    required: true,
                    maxlength: 1000,
                    minlength:10
                },
                TasaInteres: {
                    required: true,
                    digits: true
                },
                Estado: {
                    required: true

                },
                Anticipado: {
                    required: true

                },
                Comision: {
                    required: true,
                    digits:true
                }
            }
        }
    );
}

function EventoClick() {
    $("#btnModificar").on("click", function () {
        var formulario = $("#frmModifica");
        formulario.validate();

        if (formulario.valid()) {
            CapturarFormulario();
        }
    });
}
function CapturarFormulario() {
    var pId = $("#IdAhorro").text();
    var pNombre = $("#Nombre").val();
    var pDescripcion = $("#Descripcion").val();
    var pTasaInteres = $("#TasaInteres").val();
    var pEstado = $("#Estado").val();
    if (pEstado == 1) {
        pEstado = true;
    } else {
        pEstado = false;
    }
    var pAnticipado = $("#Anticipado").val();
    if (pAnticipado == 1) {
        pAnticipado = true;
    } else {
        pAnticipado = false;
    }
    var pComision = $("#Comision").val();





    var url = '/Ahorros/ActualizarCatalogoAhorroID';
    var parametros = {
        Id:pId,
        Nombre : pNombre,
        Descripcion: pDescripcion,
        TasaInteres: pTasaInteres,
        Activo: pEstado,
        Anticipado: pAnticipado,
        Comision: pComision

    };
    var funcion = MostrarResultadoModificar;
    var tipo = false;
    ejecutaAjax(url,parametros,funcion,tipo);
}

function MostrarResultadoModificar(data) {
    if (data.FilasAfectadas != 0) {
        $(location).attr('href', '/Ahorros/Index');
    } else {
        MensajeError(data.mensaje);
    }
}