﻿$(function () {
    ConsultarCatalogoAhorros();
    creaValidaciones();
    EventoClick();
});


function MensajeExito(Mensaje) {
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: Mensaje,
        showConfirmButton: false,
        timer: 3500
    })
}

function MensajeError(Mensaje) {
    Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: Mensaje,
        showConfirmButton: false,
        timer: 3500
    })
}

function EventoClick() {
    $("#btnGuardar").on("click", function () {
        var formulario = $("#frmInicioSesion");
        formulario.validate();
        if (formulario.valid()) {
            CapturarFormulario();
        }
    });
}

//Funcion captura los datos del formulario
function CapturarFormulario() {
    var pNombre = $("#Nombre").val();
    var pDescripcion = $("#Descripcion").val();
    var pTasaInteres = $("#TasaInteres").val();
    var pEstado = $("#Estado").val();
    if (pEstado == 1) {
        pEstado = true;
    } else {
        pEstado = false;
    }
    var pAnticipado = $("#Anticipado").val();
    if (pAnticipado == 1) {
        pAnticipado = true;
    } else {
        pAnticipado = false;
    }
    var pComision = $("#Comision").val();





    var url = '/Ahorros/RegistrarCatalogoAhorro';
    var parametros = {
        Nombre: pNombre,
        Descripcion: pDescripcion,
        TasaInteres: pTasaInteres,
        Activo: pEstado,
        Anticipado: pAnticipado,
        Comision: pComision

    };
    var funcion = MostrarResultadoRegistro;
    var tipo = false;
    ejecutaAjax(url, parametros, funcion, tipo);
}
function MostrarResultadoRegistro(data) {
    if (data.filas > 0) {
        MensajeExito(data.resultado);
        ConsultarCatalogoAhorros();
    } else if (data.filas == -1) {
        MensajeError(data.resultado);
    }
}
///crea las validaciones para el formulario
function creaValidaciones() {
    $("#frmInicioSesion").validate(
        ///objeto que contiene "las condiciones" que el formulario
        ///debe cumplir para ser considerado válido
        {
            rules: {
                Nombre: {
                    required: true,
                    maxlength: 1000,
                    minlength: 8
                },
                Descripcion: {
                    required: true,
                    maxlength: 1000,
                    minlength: 10
                },
                TasaInteres: {
                    required: true,
                    digits: true
                },
                Estado: {
                    required: true

                },
                Anticipado: {
                    required: true

                },
                Comision: {
                    required: true,
                    digits: true
                }
            }
        }
    );
}
//Funcion cargar en una tabla el catalogo de los clientes
function ConsultarCatalogoAhorros() {
    var url = '/Ahorros/ConsultaCatalogoAhorros';
    var parametros = {};
    var funcion = ProcesarConsultaCatalogoAhorro;
    var tipo = false;
    ejecutaAjax(url, parametros, funcion, tipo);
    
}

function ProcesarConsultaCatalogoAhorro(data) {
    var ddTabla = $("#TablaAhorros");
    ddTabla.empty();
    //cargamos el encabezado de la tabla
    ddTabla.append('<tr>'
        + '<th>Nombre</th>'
        + '<th>Descripcion</th>'
        + '<th>Tasa interes</th>'
        + '<th>Estado</th>'
        + '<th>Comisión por retiro</th>'
        + '<th>Actualizar</th>'
        + '<th>Eliminar</th>'
        + '</tr > ');

    //agregamos el detalle
    $(data.resultado).each(function () {
        var actual = this;
        var actualizar = '<a href="/Ahorros/ModificarAhorro?Id=' + actual.Id + '">Modificar</a>';
        var eliminar = '<a href="/Ahorros/EliminarAhorro?Id=' + actual.Id + '">Eliminar</a>';
        nuevaOpcion = '<tr>'
            + '<td>' + actual.Nombre + '</td>'
            + '<td>' + actual.Descripcion + '</td>'
            + '<td>' + actual.TasaInteres + '</td>'
            + '<td>' + actual.Estado + '</td>'
            + '<td>' + actual.ComisionRetiroAnticipado + '</td>'
            + '<td>' + actualizar + '</td>'
            + '<td>' + eliminar + '</td>'
            +'</tr >';
        ddTabla.append(nuevaOpcion);
    });

    
}

