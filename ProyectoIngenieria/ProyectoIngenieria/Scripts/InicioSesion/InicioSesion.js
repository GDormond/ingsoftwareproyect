﻿$(function () {
    ValidarNombreUsuario();
    ClickEnviarCorreo();
});

function Prueba(Mensaje) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: Mensaje
    })
}
//Funcion que muestra un mensaje de exito
function MensajeExito(Titulo,Mensaje) {
    Swal.fire({
        position: 'top-center',
        icon: 'success',
        title: Titulo,
        text: Mensaje,
        showConfirmButton: false,
        timer: 3000
    })
}

//Funcion que muestra un mensaje de error
function MensajeError(titulo,Mensaje) {
    Swal.fire({
        position: 'top-center',
        icon: 'error',
        title: titulo,
        text: Mensaje,
        showConfirmButton: false,
        timer: 3000
    })
}
//VALIDAR FORMULARIO
function ValidarNombreUsuario() {
    $("#frmCambioPass").validate(

        {
            rules: {
                NombreUsuario: {
                    required: true,
                    maxlength: 30,
                    minlength: 3
                }
            }

        }
    );
}




function ClickEnviarCorreo() {
    $("#btnEnviarCorreo").on("click", function () {
         var formulario = $("#btnEnviarCorreo")
        formulario.validate();
        if (formulario.valid()) {
            var Usuario = $("#NombreUsuario").val();
            var url = '/Usuario/ConsultarCorreoPorUsuario'
            var parametros = {
                NombreUsuario: Usuario
            };
            var funcion = MostrarMensajeResultado;
            var tipo = false; //Nos indica si el proceso se ejecuta de forma ASYNC
            ejecutaAjax(url, parametros, funcion, tipo);
        }
         
    });
}

//Mostramos el resultado
function MostrarMensajeResultado(data) {
    var titulo;
    var Mensaje;
    if (data.resultado == "Exito") {
        titulo = "Correo electrónico";
        Mensaje = "Se envío la nueva contraseña al correo registrado";
        //Se envía el mensaje para el usuario
        //MensajeExito(titulo, Mensaje);
        Prueba(Mensaje);
    } else if (data.resultado == "Error") {
        titulo = "Error..";
        Mensaje = "Hubo un error al ejecutar la consulta por favor reportar al departamento de TI";
        //Se envía el mensaje para el usuario
        MensajeError(titulo, Mensaje);
    } else {
        titulo = "Cuenta de usuario";
        Mensaje = "Usuario no existe";
        //Se envía el mensaje para el usuario
        MensajeError(titulo, Mensaje);
    }
}