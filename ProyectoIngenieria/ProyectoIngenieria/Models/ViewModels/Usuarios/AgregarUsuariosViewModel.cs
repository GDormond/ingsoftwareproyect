﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProyectoIngenieria.Models.ViewModels.Usuarios {
    public class AgregarUsuariosViewModel {
        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }

        [DisplayName("Primer apellido")]
        [Required(ErrorMessage = "El primer apellido es requerido")]
        public string Apellido1 { get; set; }

        [DisplayName("Segundo apellido")]
        [Required(ErrorMessage = "El segundo apellido es requerido")]
        public string Apellido2 { get; set; }

        [DisplayName("Cédula de identidad")]
        [Required(ErrorMessage = "La cédula de identidad es requerida")]
        public string Cedula { get; set; }

        [DisplayName("Correo electrónico")]
        [Required(ErrorMessage = "El correo electrónico es requerido")]
        [EmailAddress(ErrorMessage = "Debe ingresar un correo válido")]
        public string Correo { get; set; }

        [DisplayName("Teléfono")]
        [Required(ErrorMessage = "El teléfono es requerido")]
        public string Telefono { get; set; }

        [DisplayName("Nombre de usuario")]
        [Required(ErrorMessage = "El nombre de usuario es requerido")]
        public string NombreUsuario { get; set; }

        [DisplayName("Tipo de usuario")]
        [Required(ErrorMessage = "Debe seleccionar un tipo de usuario")]
        public int TipoUsuario { get; set; }

        [DisplayName("Contraseña")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "La contraseña es requerida")]
        public string Contrasena { get; set; }

        [DisplayName("Confirmar contraseña")]
        [DataType(DataType.Password)]
        [Compare("Contrasena", ErrorMessage = "Las contraseñas no coinden")]
        public string ConfirmarContrasena { get; set; }
    }
}