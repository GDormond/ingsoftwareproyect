﻿using ProyectoIngenieria.Models.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ProyectoIngenieria.Models.ViewModels.Usuarios {
    public class IndexUsuariosViewModel {
        public IndexUsuariosViewModel(TBL_PERSONAS Entidad) {
            IdUsuario = Entidad.TBL_USUARIOS.FirstOrDefault().Id;
            IdPersona = Entidad.Id;
            NombreCompleto = $"{Entidad.Nombre} {Entidad.Apellido1} {Entidad.Apellido2}";
            Estado = Convert.ToBoolean(Entidad.TBL_USUARIOS.FirstOrDefault().Activo) ? "Activo" : "Inactivo";
            Correo = Entidad.Email;
            Telefono = Entidad.Telefono1;
            NombreUsuario = Entidad.TBL_USUARIOS.FirstOrDefault().Nombre;
            TipoUsuario = Entidad.TBL_USUARIOS.FirstOrDefault().TBL_TIPOS_USUARIO.Descripcion;
        }

        public int IdUsuario { get; set; }

        public int IdPersona { get; set; }

        [DisplayName("Nombre completo")]
        public string NombreCompleto { get; set; }

        public string Estado { get; set; }

        [DisplayName("Correo electrónico")]
        public string Correo { get; set; }

        [DisplayName("Teléfono")]
        public string Telefono { get; set; }

        [DisplayName("Nombre de usuario")]
        public string NombreUsuario { get; set; }

        [DisplayName("Tipo de usuario")]
        public string TipoUsuario { get; set; }
    }
}