﻿using ProyectoIngenieria.Models.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProyectoIngenieria.Models.ViewModels.Usuarios {
    public class EditarUsuariosViewModel {
        public EditarUsuariosViewModel() { }

        public EditarUsuariosViewModel(TBL_USUARIOS entidad) {
            IdUsuario = entidad.Id;
            IdPersona = Convert.ToInt16(entidad.IdPersona);
            Nombre = entidad.TBL_PERSONAS.Nombre;
            Apellido1 = entidad.TBL_PERSONAS.Apellido1;
            Apellido2 = entidad.TBL_PERSONAS.Apellido2;
            Cedula = entidad.TBL_PERSONAS.Cedula;
            Correo = entidad.TBL_PERSONAS.Email;
            Telefono = entidad.TBL_PERSONAS.Telefono1;
            NombreUsuario = entidad.Nombre;
            TipoUsuario = Convert.ToInt16(entidad.IdTipoUsuario);
            ContrasenaActual = entidad.Pass;
        }

        public int IdUsuario { get; set; }

        public int IdPersona { get; set; }

        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }

        [DisplayName("Primer apellido")]
        [Required(ErrorMessage = "El primer apellido es requerido")]
        public string Apellido1 { get; set; }

        [DisplayName("Segundo apellido")]
        [Required(ErrorMessage = "El segundo apellido es requerido")]
        public string Apellido2 { get; set; }

        [DisplayName("Cédula de identidad")]
        [Required(ErrorMessage = "La cédula de identidad es requerida")]
        public string Cedula { get; set; }

        [DisplayName("Correo electrónico")]
        [Required(ErrorMessage = "El correo electrónico es requerido")]
        [EmailAddress(ErrorMessage = "Debe ingresar un correo válido")]
        public string Correo { get; set; }

        [DisplayName("Teléfono")]
        [Required(ErrorMessage = "El teléfono es requerido")]
        public string Telefono { get; set; }

        [DisplayName("Nombre de usuario")]
        [Required(ErrorMessage = "El nombre de usuario es requerido")]
        public string NombreUsuario { get; set; }

        [DisplayName("Tipo de usuario")]
        [Required(ErrorMessage = "Debe seleccionar un tipo de usuario")]
        public int TipoUsuario { get; set; }

        public string ContrasenaActual { get; set; }

        [DisplayName("Nueva contraseña")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "La contraseña es requerida")]
        public string Contrasena { get; set; }

        [DisplayName("Confirmar nueva contraseña")]
        [DataType(DataType.Password)]
        [Compare("Contrasena", ErrorMessage = "Las contraseñas no coinden")]
        public string ConfirmarContrasena { get; set; }
    }
}