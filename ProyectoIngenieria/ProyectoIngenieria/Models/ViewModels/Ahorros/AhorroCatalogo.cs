﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProyectoIngenieria.Models.ViewModels.Ahorros
{
    public class AhorroCatalogo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion {get;set;}
        public double TasaInteres {get;set;}

        public Boolean Activo { get; set; }
        public Boolean Anticipado { get; set; }
        public decimal Comision { get; set; }
    }
}