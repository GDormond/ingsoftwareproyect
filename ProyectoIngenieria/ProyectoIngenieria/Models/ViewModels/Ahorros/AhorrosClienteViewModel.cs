﻿using ProyectoIngenieria.Models.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ProyectoIngenieria.Models.ViewModels.Ahorros {
    public class AhorrosClienteViewModel {
        public AhorrosClienteViewModel(TBL_AHORROS_CLIENTE Entidad) {
            Id = Entidad.Id;
            NombreAhorro = Entidad.TBL_CATALOGO_AHORROS.Nombre;
            Vencimiento = Convert.ToDateTime(Entidad.FechaVencimiento).ToString("dd/MM/yyyy");
            Tasa = Entidad.TBL_CATALOGO_AHORROS.TasaInteres;
            AporteAcordado = Entidad.AporteAcordado;
            Saldo = Entidad.SaldoActual;
            MontoUltimoAporte = Entidad.MontoUltimoAporte;
            UltimoAporte = Convert.ToDateTime(Entidad.FechaUltimoAporte).ToString("dd/MM/yyyy");
            Observaciones = Entidad.Observaciones;
        }

        public int Id { get; set; }

        [DisplayName("Ahorro")]
        public string NombreAhorro { get; set; }

        public string Vencimiento { get; set; }

        [DisplayName("Tasa de interés")]
        public double Tasa { get; set; }

        [DisplayName("Aporte acordado")]
        public decimal AporteAcordado { get; set; }

        [DisplayName("Saldo a la fecha")]
        public decimal Saldo { get; set; }

        [DisplayName("Ultimo aporte")]
        public decimal MontoUltimoAporte { get; set; }

        [DisplayName("Último aporte")]
        public string UltimoAporte { get; set; }

        public string Observaciones { get; set; }
    }
}