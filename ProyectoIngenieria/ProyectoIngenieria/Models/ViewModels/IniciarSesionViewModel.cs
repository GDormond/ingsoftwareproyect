﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProyectoIngenieria.Models.ViewModels {
    public class IniciarSesionViewModel {
        public string NombreUsuario { get; set; }
        public string Contrasena { get; set; }
    }
}