﻿using ProyectoIngenieria.Models.EntityFramework;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ProyectoIngenieria.Models.ViewModels.Creditos {
    public class EditarCreditosViewModel {
        public EditarCreditosViewModel() { }

        public EditarCreditosViewModel(TBL_CATALOGO_CREDITOS entidad) {
            Id = entidad.Id;
            Nombre = entidad.Nombre;
            Descripcion = entidad.Descripcion;
            Tasa = entidad.Tasa;
            Plazo = entidad.PlazoMaximoEnMeses;
            Estado = entidad.Activo;
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }

        [DisplayName("Descripción")]
        [Required(ErrorMessage = "La descripcion es requerida")]
        public string Descripcion { get; set; }

        [DisplayName("Tasa de interés")]
        [Required(ErrorMessage = "La tasa es requerida")]
        public double Tasa { get; set; }

        [DisplayName("Plazo (En meses)")]
        [Required(ErrorMessage = "El plazo es requerido")]
        public int Plazo { get; set; }

        [DisplayName("Activo")]
        public bool Estado { get; set; }
    }
}