﻿using ProyectoIngenieria.Models.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ProyectoIngenieria.Models.ViewModels.Creditos {
    public class IndexCreditosViewModel {
        public IndexCreditosViewModel(TBL_CATALOGO_CREDITOS Entidad) {
            Id = Entidad.Id;
            Nombre = Entidad.Nombre;
            Descripcion = Entidad.Descripcion;
            Tasa = Entidad.Tasa;
            Plazo = Entidad.PlazoMaximoEnMeses;
            Estado = Entidad.Activo ? "Activo" : "Inactivo";
        }

        public int Id { get; set; }

        public string Nombre { get; set; }

        [DisplayName("Descripción")]
        public string Descripcion { get; set; }

        [DisplayName("Tasa de interés")]
        public double Tasa { get; set; }

        [DisplayName("Plazo máximo en meses")]
        public int Plazo { get; set; }

        public string Estado { get; set; }
    }
}