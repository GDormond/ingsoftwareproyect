﻿using ProyectoIngenieria.Models.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ProyectoIngenieria.Models.ViewModels.Creditos {
    public class CreditosClienteViewModel {
        public CreditosClienteViewModel(TBL_CREDITOS_CLIENTE Entidad) {
            Id = Entidad.Id;
            NombreCredito = Entidad.TBL_CATALOGO_CREDITOS.Nombre;
            Tasa = Entidad.TBL_CATALOGO_CREDITOS.Tasa;
            Monto = Entidad.MontoTotal;
            Plazo = Entidad.Plazo;
            Cuota = Entidad.Cuota;
            SaldoActual = Entidad.SaldoActual;
            PlazoRestante = Entidad.PlazoRestante;
            MontoUltimoPago = Entidad.MontoUltimoPago;
            UltimoPago = Convert.ToDateTime(Entidad.FechaUltimoPago).ToString("dd/MM/yyyy");
            Observaciones = Entidad.Observaciones;
        }

        public int Id { get; set; }

        [DisplayName("Crédito")]
        public string NombreCredito { get; set; }

        [DisplayName("Tasa de interés")]
        public double Tasa { get; set; }

        [DisplayName("Monto")]
        public decimal Monto { get; set; }

        [DisplayName("Plazo del crédito")]
        public int Plazo { get; set; }

        public decimal Cuota { get; set; }

        [DisplayName("Saldo")]
        public decimal SaldoActual { get; set; }

        [DisplayName("Plazo restante")]
        public int PlazoRestante { get; set; }

        [DisplayName("Ultimo pago")]
        public decimal MontoUltimoPago { get; set; }

        [DisplayName("Último pago")]
        public string UltimoPago { get; set; }

        public string Observaciones { get; set; }
    }
}