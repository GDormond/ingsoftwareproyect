-- =============================================
-- Author:		<ANDREY AZOFEIFA ALVARADO>
-- Create date: <09/11/2020>
-- Description:	<RETORNA LOS DATOS DE UN USUARIO>
-- =============================================
CREATE PROCEDURE SP_ConsultarInfoUsuario
	@Id INT
AS
BEGIN
    SELECT  U.Id AS IdUsuario,
            U.Nombre,       
            P.Nombre,
            P.Apellido1,
            P.Apellido2,
            P.Cedula,
            P.Email,
            P.Id AS IdPersona,
            TU.IdTipo,
            TU.DescripcionTipo
    FROM TBL_USUARIOS U
    INNER JOIN TBL_PERSONA P ON U.IdPersona = P.Id
    INNER JOIN TBL_TIPO_USUARIO TU ON TU.IdTipo = U.IdTipoUsuario
    WHERE U.Id = @Id
END
