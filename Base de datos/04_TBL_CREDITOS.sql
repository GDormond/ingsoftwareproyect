-- Tabla con el listado/catalogo de creditos deisponibles para los clientes
CREATE TABLE TBL_CATALOGO_CREDITOS (
    Id INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    Nombre NVARCHAR(255) NOT NULL,
    Descripcion NVARCHAR(1000) NOT NULL,
    Tasa FLOAT NOT NULL,
    PlazoMaximoEnMeses INT NOT NULL,
    Activo BIT NOT NULL
)