CREATE TABLE TBL_PERSONAS (
	Id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	Cedula char(9) NOT NULL UNIQUE,
	Nombre VARCHAR(60) NOT NULL,
	Apellido1 VARCHAR(60) NOT NULL,
	Apellido2 VARCHAR(60) NOT NULL,
	Email VARCHAR(60) NOT NULL UNIQUE,
	Telefono1 NVARCHAR(10) NOT NULL,
	Telefono2 NVARCHAR(10) NULL
)