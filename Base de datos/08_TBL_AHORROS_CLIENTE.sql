-- Tabla que contiene los ahorros asignados a un cliente
CREATE TABLE TBL_AHORROS_CLIENTE (
    Id INT NOT NULL PRIMARY KEY,
    IdAhorro INT NOT NULL FOREIGN KEY REFERENCES TBL_CATALOGO_AHORROS(Id),
    IdCliente INT NOT NULL FOREIGN KEY REFERENCES TBL_PERSONAS(Id),
    FechaVencimiento DATETIME NULL,
    AporteAcordado DECIMAL (18, 2) NOT NULL,
    SaldoActual DECIMAL (18, 2) NOT NULL,
    MontoUltimoAporte DECIMAL (18, 2) NOT NULL,
    FechaUltimoAporte DATETIME NULL,
    Observaciones NVARCHAR(1000) NOT NULL
)