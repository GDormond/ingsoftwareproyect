-- Tabla con listado de ahorros disponibles para los clientes
CREATE TABLE TBL_CATALOGO_AHORROS (
    Id INT IDENTITY(1, 1) NOT NULL PRIMARY KEY,
    Nombre NVARCHAR(255) NOT NULL,
    Descripcion NVARCHAR(1000) NOT NULL,
    TasaInteres FLOAT NOT NULL,
    Activo BIT NOT NULL,
    PermiteRetiroAnticipado BIT NOT NULL,
    ComisionRetiroAnticipado DECIMAL(18, 2) NOT NULL
)
