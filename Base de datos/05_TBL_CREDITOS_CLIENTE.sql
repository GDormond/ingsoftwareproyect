-- Tabla que contiene los créditos otorgados a un cliente
CREATE TABLE TBL_CREDITOS_CLIENTE (
    Id INT NOT NULL PRIMARY KEY,
    IdCredito INT NOT NULL FOREIGN KEY REFERENCES TBL_CATALOGO_CREDITOS(Id),
    IdCliente INT NOT NULL FOREIGN KEY REFERENCES TBL_PERSONAS(Id),
    MontoTotal DECIMAL (18, 2) NOT NULL,
    Plazo INT NOT NULL,
    FechaVencimiento DATETIME NOT NULL,
    Cuota DECIMAL (18, 2) NOT NULL,
    SaldoActual DECIMAL (18, 2) NOT NULL,
    MontoUltimoPago DECIMAL (18, 2) NOT NULL,
    FechaUltimoPago DATETIME NULL,
    PlazoRestante INT NOT NULL,
    Observaciones NVARCHAR(1000) NOT NULL
)