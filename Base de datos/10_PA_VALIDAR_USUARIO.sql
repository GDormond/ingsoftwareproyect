-- =============================================
-- Author:		<ANDREY AZOFEIFA ALVARADO>
-- Create date: <09/11/2020>
-- Description:	<RETORNA LOS DATOS DE UN USUARIO>
-- =============================================
CREATE PROCEDURE SP_ValidarUsuario
	@Usuario VARCHAR(60),
	@Pass VARCHAR(500)
AS
BEGIN
    IF EXISTS (SELECT Id FROM TBL_USUARIOS WHERE Nombre = @Usuario AND Pass = @Pass)
    BEGIN
        SELECT Id FROM TBL_USUARIOS WHERE Nombre = @Usuario AND Pass = @Pass
    END
    ELSE
    BEGIN
        SELECT 0 AS Id
    END
END