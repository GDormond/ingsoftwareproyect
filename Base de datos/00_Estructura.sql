-- Personas clientes y colaboradores registrados en el sistema
SELECT * FROM TBL_PERSONAS -- Incluir campos de provincia, canton, distrito y direccion extacta, quitar referencia a tipo_persona

-- Tipos de usuario que utilizarán el sistema
SELECT * FROM TBL_TIPO_USUARIO

-- Usuarios registrados para utilizar el sistema
SELECT * FROM TBL_USUARIOS

-- Tabla con el listado/catalogo de creditos deisponibles para los clientes
SELECT * FROM TBL_CATALOGO_CREDITOS

-- Tabla con listado de ahorros disponibles para los clientes
SELECT * FROM TBL_CATALOGO_AHORROS

-- Tabla con los creditos de los clientes
SELECT * FROM TBL_CREDITOS_CLIENTE

-- Tabla con los ahorros de los clientes
SELECT * FROM TBL_AHORROS_CLIENTE

-- Histórico de pagos a los creditos de un cliente
SELECT * FROM TBL_PAGOS_CREDITO

-- Histórico de aportaciones a los ahorros de un cliente
SELECT * FROM TBL_APORTES_AHORRO
